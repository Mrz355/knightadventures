﻿using System.Windows.Controls;

namespace KnightAdventures
{
    class Monster
    {
        public int attack { get; }
        public int hp { get; set; }
        public Image img { get; set; }

        public Monster(int attack, int hp, Image img)
        {
            this.attack = attack;
            this.hp = hp;
            this.img = img;
        }
    }
}
