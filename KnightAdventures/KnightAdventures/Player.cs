﻿using System.Collections.Generic;
using System.Windows.Controls;

namespace KnightAdventures
{
    public class Player
    {
        public int x { get; set; }
        public int y { get; set; }
        MainWindow mainWindow;
        Image knight;
        public Dictionary<string, Weapon> inventory;
        public int attack { get; set; }
        private int actual_hp;
        public int hp { get
            {
                return this.actual_hp;
            } set
            {
                this.actual_hp = value;
                HPLabel.Content = "Your HP: " + value + "%";
            }
        }
        private int _coins;
        public int coins
        {
            get
            {
                return _coins;
            }
            set
            {
                _coins = value;
                CoinsLabel.Content = "Coins: " + value;
                CoinsBlacksmithLabel.Content = "Your coins: " + value;
            }
        }
        private int _potions;
        public int potions
        {
            get { return this._potions; }
            set
            {
                this._potions = value;
                PotionsLabel.Content = "Potions amount: " + value;
            }
        }
        private int _keys;
        public int keys
        {
            get { return _keys; }
            set
            {
                _keys = value;
                mainWindow.KeysLabel.Content = "Keys: " + value;
            }
        }
        public bool spokenToDrunkMan;
        public bool haveBeer;

        private Label attackLabel;
        private Label HPLabel;
        private Label PotionsLabel;
        private Label CoinsLabel;
        private Label CoinsBlacksmithLabel;

        public Player(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
            this.knight = mainWindow.Knight;
            inventory = new Dictionary<string, Weapon>();
            this.x = 2;
            this.y = 7;

            this.attackLabel = mainWindow.attackLabel;
            this.HPLabel = mainWindow.HPLabel;
            this.PotionsLabel = mainWindow.PotionsLabel;
            this.CoinsLabel = mainWindow.CoinsLabel;
            this.CoinsBlacksmithLabel = mainWindow.BlacksmithCoinsLabel;

            hp = 100;
            spokenToDrunkMan = false;
            haveBeer = false;
        }

        public void setPosition(int x, int y)
        {
            this.x = x;
            this.y = y;
            Grid.SetRow(knight, x);
            Grid.SetColumn(knight, y);
        }

        internal void addToInventory(string weaponName, Weapon weapon)
        {
            mainWindow.inventory.Add(weaponName, weapon);
            mainWindow.UpdateInventory();
            //attack += weapon.powerUp;
            int max_attack = 0;
            foreach(Weapon w in mainWindow.inventory.Values)
            {
                if (w.powerUp > max_attack) max_attack = w.powerUp;
            }
            attack = max_attack;
            attackLabel.Content = "Your attack is: " + attack;
        }
        internal void addHP(int hp)
        {
            this.hp += hp;
            HPLabel.Content = "Your HP is: " + this.hp +"%";
        }
    }
}