﻿using System;

namespace KnightAdventures
{
    public class Weapon
    {
        public string name {get; }
        public string prefix { get; }
        public string type {get;}
        public string enchant {get;}
        public int powerUp { get; }
        public int price { get; }

        public Weapon(string name, string prefix, string type, string enchant, int powerUp, int price)
        {
            this.name = name;
            this.prefix = prefix;
            this.type = type;
            this.enchant = enchant;
            this.powerUp = powerUp;
            this.price = price;
        }
        override
        public String ToString()
        {
            String res = String.Format("Name : {0} {1} (+{2})\nType : {3}\nEnchant : {4}\n", prefix, name, powerUp, type, enchant);
            return res;
        }
    }
}
