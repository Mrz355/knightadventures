﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace KnightAdventures
{
    public partial class MainWindow : Window
    {
        Button[,] buttonList;
        public Field[,] fieldList { get; set; }
        Game game;
        private List<string> weaponEnchants;
        public List<string> weaponNames { get; set; }
        private List<string> weaponPrefixes;
        private List<string> weaponPrices;
        private List<string> weaponTypes;
        public List<Door> doors;
        public List<ClosedDoor> closedDoors;
        public List<Button1> buttons1;
        public Button2 button2;
        public WisdomMan wisdomMan { get; set; }
        internal DrunkMan drunkMan;
        //internal List<Monster> monsterList;
        internal Dictionary<Coords, Monster> monsterList;
        public Dictionary<string, Weapon> blacksmith { get; set; }
        public Dictionary<string, Weapon> lootWeapons { get; set; }
        public Dictionary<string, Weapon> inventory { get; set; }
        internal Fight fight { get; set; }
        private Timer timer;

        // For arrow keys handling
        bool left_enabled = true;
        bool right_enabled = true;
        bool up_enabled = true;
        bool down_enabled = true;
        bool space_enabled = true;

        Random r = new Random();
        
        public MainWindow()
        {
            InitializeComponent();
            InitializeButtons();
            InitializeLists();
            CreateDoors();
            CreateMonsters();
            CreateClosedDoors();
            CreateButtons1();
            CreateButtons2();
            HideAreas();
            SmithWeapons();
            InitializeWeaponTypesCB();
            InitializeInventory();
            InitializeBlacksmith();

            wisdomMan = new WisdomMan(this);
            drunkMan = new DrunkMan(this);
        }

        private void InitializeInventory()
        {
            Grid.SetZIndex(InventoryGrid, -1);
        }

        private void SmithWeapons()
        {
            blacksmith = new Dictionary<string, Weapon>();
            inventory = new Dictionary<string, Weapon>();
            lootWeapons = new Dictionary<string, Weapon>();
            Random r = new Random();
            int prefixesLength = weaponPrefixes.Count;
            int typesLength = weaponTypes.Count;
            int enchantLength = weaponEnchants.Count;
            int namesLength = weaponNames.Count;
            for (int i=0;i<namesLength/2;++i)
            {
                String s = weaponNames[i];
                int powerUp = r.Next(6,17);
                blacksmith.Add(s, new Weapon(s, GetRandomElement(weaponPrefixes), GetRandomElement(weaponTypes), GetRandomElement(weaponEnchants), powerUp,(r.Next(powerUp/2*5,powerUp*10)+7)));
            }
            for(int i=namesLength/2;i<namesLength;++i)
            {
                String s = weaponNames[i];
                int powerUp = r.Next(1,6);
                lootWeapons.Add(s, new Weapon(s, GetRandomElement(weaponPrefixes), GetRandomElement(weaponTypes), GetRandomElement(weaponEnchants), powerUp, (r.Next(powerUp / 2 * 5, powerUp * 10)+7)));
            }
        }
        public string GetRandomElement(List<string> list)
        {
            return list[r.Next(list.Count)];
        }
        private void InitializeLists()
        {
            try
            {
                weaponNames = File.ReadAllLines("weaponNames.txt").ToList();
                weaponPrefixes = File.ReadAllLines("weaponPrefix.txt").ToList();
                weaponTypes = File.ReadAllLines("weaponType.txt").ToList();
                weaponEnchants = File.ReadAllLines("weaponEnchant.txt").ToList();
                weaponEnchants = weaponEnchants[0].Split(',').ToList();
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e.Message);
            }
        }
        // Adding 25 * 31 buttons to the board :)
        private void InitializeButtons()
        {
            // ADDING BUTTONS 
            buttonList = new Button[21, 35];
            Button button;
            for (int i = 0; i < 21; ++i)
            {
                for (int j = 0; j < 35; ++j)
                {
                    button = new Button();
                    button.IsEnabled = true; // <----- FOR DEBBUGING PURPOSE CHANGE THIS
                    button.Opacity = 0.02;
                    button.Click += new RoutedEventHandler(onButtonClick);
                    button.Focusable = false;

                    buttonList[i, j] = button;   // Table containing every button

                    Grid.SetRow(button, i);
                    Grid.SetColumn(button, j);
                    Board.Children.Add(button);
                }
            }
            // BUTTONS ADDED
            // Now we specyfiy buttons behaviour
            fieldList = new Field[21, 35];
            for (int i = 0; i < 21; ++i) for (int j = 0; j < 35; ++j) fieldList[i, j] = new Field();       // Initializes the map with specific fields with special behaviours (for eg. WALL, ENEMY)
            // Everything is FLOOR now, let's change it
            // Firstly, the frame needs to be a WALL:
            for (int j = 0; j < 35; ++j)
            {
                fieldList[0, j].state = State.WALL;
                fieldList[20, j].state = State.WALL;
            }
            for (int i = 1; i < 20; ++i)
            {
                fieldList[i, 0].state = State.WALL;
                fieldList[i, 34].state = State.WALL;
            }
            // Okay, now the labour work begins...

            // *****WALLS*****
            fieldList[1, 2].state = State.WALL;
            fieldList[2, 2].state = State.WALL;
            for (int i = 1; i <= 4; ++i) fieldList[i, 4].state = State.WALL;
            for (int i = 4; i <= 8; ++i) fieldList[i, 6].state = State.WALL;
            fieldList[4, 5].state = State.WALL;

            fieldList[4, 1].state = State.WALL;
            fieldList[4, 2].state = State.WALL;

            for (int i = 6; i <= 9; ++i) fieldList[i, 2].state = State.WALL;
            fieldList[9, 1].state = State.WALL;
            for (int i = 4; i <= 12; ++i) fieldList[9, i].state = State.WALL;
            fieldList[8, 4].state = State.WALL;
            fieldList[6, 4].state = State.WALL;
            fieldList[6, 5].state = State.WALL;
            for (int i = 7; i <= 12; ++i) fieldList[7, i].state = State.WALL;
            for (int i = 1; i <= 6; ++i) fieldList[i, 10].state = State.WALL;
            for (int i = 4; i <= 16; ++i) fieldList[i, 13].state = State.WALL;
            for (int i = 1; i <= 12; ++i) fieldList[13, i].state = State.WALL;
            fieldList[3, 11].state = State.WALL;

            for (int i = 1; i <= 11; ++i) fieldList[15, i].state = State.WALL;
            for (int i = 16; i <= 19; ++i) fieldList[i, 4].state = State.WALL;
            for (int i = 16; i <= 19; ++i) fieldList[i, 8].state = State.WALL;
            for (int i = 16; i <= 19; ++i) fieldList[i, 11].state = State.WALL;
            fieldList[10, 9].state = State.WALL;
            fieldList[12, 9].state = State.WALL;
            for (int i = 14; i <= 19; ++i) fieldList[10, i].state = State.WALL;
            for (int i = 12; i <= 19; ++i) fieldList[i, 19].state = State.WALL;
            for (int i = 11; i <= 16; ++i) fieldList[i, 17].state = State.WALL;
            fieldList[12, 14].state = State.WALL;
            fieldList[12, 16].state = State.WALL;
            fieldList[16, 15].state = State.WALL;
            fieldList[16, 16].state = State.WALL;

            for (int i = 1; i <= 19; ++i) fieldList[i, 24].state = State.WALL;
            for (int i = 20; i <= 22; ++i) fieldList[14, i].state = State.WALL;
            for (int i = 1; i <= 3; ++i) fieldList[i, 15].state = State.WALL;
            for (int i = 1; i <= 3; ++i) fieldList[i, 21].state = State.WALL;
            for (int i = 15; i <= 21; ++i) fieldList[4, i].state = State.WALL;

            for (int i = 6; i <= 8; ++i)
            {
                for (int j = 20; j <= 22; ++j)
                {
                    fieldList[i, j].state = State.WALL;
                }
            }
            // *****END OF WALLS*****

            fieldList[1,1].state = State.BUTTON1; 
            fieldList[1,3].state = State.BUTTON1;
            fieldList[5,5].state = State.BUTTON1;
            fieldList[8,1].state = State.BUTTON1;
            fieldList[8,5].state = State.BUTTON1;

            fieldList[10,11].state = State.BUTTON2;
            fieldList[11,12].state = State.BUTTON2;
            fieldList[12,11].state = State.BUTTON2;

            fieldList[1, 5].state = State.LOOT;
            fieldList[2, 9].state = State.LOOT;
            fieldList[3, 20].state = State.LOOT;
            fieldList[5,17].state = State.LOOT;
            fieldList[6,7].state = State.LOOT;
            fieldList[8,7].state = State.LOOT;
            fieldList[9,16].state = State.LOOT;
            fieldList[10,5].state = State.LOOT;
            fieldList[10,6].state = State.LOOT;
            fieldList[10,7].state = State.LOOT;
            fieldList[10,8].state = State.LOOT;
            fieldList[11,1].state = State.LOOT;

            fieldList[11,16].state = State.LOOT;
            fieldList[12,1].state = State.LOOT;
            fieldList[13,20].state = State.LOOT;
            fieldList[15,16].state = State.LOOT;
            fieldList[18,5].state = State.LOOT;
            fieldList[18,18].state = State.LOOT;
            fieldList[19,10].state = State.LOOT;
            fieldList[19,13].state = State.LOOT;
            fieldList[19,14].state = State.LOOT;
            fieldList[19,17].state = State.LOOT;
            fieldList[19,18].state = State.LOOT;

            fieldList[11, 14].state = State.KEYLOOT;
            fieldList[18, 2].state = State.KEYLOOT;

            fieldList[1, 21].state = State.OPENED_DOOR;
            fieldList[4,3].state = State.OPENED_DOOR;
            fieldList[4,14].state = State.OPENED_DOOR;
            fieldList[5,2].state = State.OPENED_DOOR;
            fieldList[7,4].state = State.OPENED_DOOR;
            fieldList[7,8].state = State.OPENED_DOOR;
            fieldList[8,13].state = State.OPENED_DOOR;
            fieldList[9,3].state = State.OPENED_DOOR;
            fieldList[11,19].state = State.OPENED_DOOR;
            fieldList[13,2].state = State.OPENED_DOOR;
            fieldList[15,2].state = State.FLOOR;
            fieldList[15,6].state = State.FLOOR;
            fieldList[15,10].state = State.FLOOR;
            fieldList[16,18].state = State.OPENED_DOOR;
            fieldList[14, 23].state = State.OPENED_DOOR;

            fieldList[1, 22].state = State.ENEMY;
            fieldList[2, 1].state = State.ENEMY;
            fieldList[2, 3].state = State.ENEMY;
            fieldList[5,4].state = State.ENEMY;
            fieldList[5,14].state = State.ENEMY;
            fieldList[6, 1].state = State.ENEMY;
            fieldList[7,5].state = State.ENEMY;
            fieldList[8,11].state = State.ENEMY;
            fieldList[10,3].state = State.ENEMY;
            fieldList[11,10].state = State.ENEMY;
            fieldList[12,15].state = State.ENEMY;
            fieldList[14, 5].state = State.ENEMY;
            fieldList[14,18].state = State.ENEMY;
            fieldList[16,12].state = State.ENEMY;
            fieldList[17,1].state = State.ENEMY;
            fieldList[17,9].state = State.ENEMY;
            fieldList[18,7].state = State.ENEMY;

            // Previously boss hearts
            fieldList[3, 30].state = State.ENEMY;
            fieldList[6,29].state = State.ENEMY;
            fieldList[10,28].state = State.ENEMY;
            fieldList[14,29].state = State.ENEMY;
            fieldList[17,30].state = State.ENEMY;

            for (int i = 3; i <= 17; ++i) fieldList[i, 33].state = State.BOSS;
            for (int i = 4; i <= 16; ++i) fieldList[i, 32].state = State.BOSS;
            for (int i = 6; i <= 14; ++i) fieldList[i, 31].state = State.BOSS;
            for (int i = 8; i <= 12; ++i) fieldList[i, 30].state = State.BOSS;

            fieldList[10, 24].state = State.CLOSED_DOOR;
            fieldList[11, 9].state = State.CLOSED_DOOR;
            fieldList[16, 14].state = State.CLOSED_DOOR;

            fieldList[2,17].state = State.WISDOM_MAN; // All NPC's will be commented out - we need to figure out how to manage them first
            fieldList[3,12].state = State.SELLER_MAN;
            fieldList[15, 20].state = State.DRUNK_MAN;

            //fieldList[2,7].state = State.START; // Don't know if make this field available

            //fieldList[14,23].state = State.STH; // Grey field, dunno what is this (Secret?)//

            
        }

        private void onButtonClick(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            int x=-1, y=-1;
            // We seek for clicked button
            for(int i=0;i<21;++i)
            {
                for(int j=0;j<35;++j)
                {
                    if(buttonList[i,j].Equals(button))
                    {
                        // Found!
                        x = i;
                        y = j;
                    }
                }
            }
            Console.WriteLine(fieldList[x,y].state); //DEBBUGING PURPOSES
            game.ButtonClicked(x, y); // The game is checking the button and doing interactions
        }
        private void InitializeWeaponTypesCB()
        {
            InventoryCB.Items.Add("All");
            foreach (string type in weaponTypes)
            {
                InventoryCB.Items.Add(type);
            }
        }
        private void inventory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (InventoryListBox.SelectedItem != null)
            {
                label.Content = (InventoryListBox.SelectedItem as Weapon).name + " of " + (InventoryListBox.SelectedItem as Weapon).enchant;
                StatsTextBlock.Text = (InventoryListBox.SelectedItem as Weapon).ToString();
            }
        }

        private void inventoryBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateInventory();
        }

        internal void UpdateInventory()
        {
            if (InventoryCB.SelectedItem != null) // No time for seeking why it becomes null at start..
            {
                if (InventoryCB.SelectedItem.ToString().Equals("All"))
                {
                    InventoryListBox.ItemsSource = null;  // Without zeroing it wasn't working
                    InventoryListBox.ItemsSource = inventory.Values;
                }
                else
                {
                    var typeDictionary = from item
                                         in inventory
                                         where InventoryCB.SelectedItem.ToString() == item.Value.type
                                         select item;
                    List<Weapon> s = new List<Weapon>();
                    foreach (var num in typeDictionary)
                    {
                        s.Add(num.Value);
                    }
                    InventoryListBox.ItemsSource = s;

                }
            }
        }
        private void CreateButtons1()
        {
            buttons1 = new List<Button1>();
            buttons1.Add(new Button1(1, 1,this));
            buttons1.Add(new Button1(1, 3,this));
            buttons1.Add(new Button1(5, 5, this));
            buttons1.Add(new Button1(8, 1, this));
            buttons1.Add(new Button1(8, 5, this));
        }

        private void CreateButtons2()
        {
            button2 = new Button2(this);
        }

        private void CreateDoors()
        {
            doors = new List<Door>();
            doors.Add(new Door(1, 21, door7, area4));
            doors.Add(new Door(4, 3, door6, area12));
            doors.Add(new Door(4, 14, door2, area3));
            doors.Add(new Door(5, 2, door10, area13));
            doors.Add(new Door(7, 4, door11, area11));
            doors.Add(new Door(7, 8, door1, area1));
            doors.Add(new Door(8, 13, door9, area2));
            doors.Add(new Door(9, 3, door5, area10));
            doors.Add(new Door(11, 19, door8, area5));
            doors.Add(new Door(13, 2, door4, area9));
            doors.Add(new Door(16, 18, door3, area6));
            doors.Add(new Door(14, 23, door12, area14));
        }

        private void CreateClosedDoors()
        {
            closedDoors = new List<ClosedDoor>();
            closedDoors.Add(new ClosedDoor(10, 24, closedDoor3, area15));
            closedDoors.Add(new ClosedDoor(11, 9, closedDoor1, area8));
            closedDoors.Add(new ClosedDoor(16, 14, closedDoor2, area7));
        }

        private void HideAreas()
        {
            area1.Visibility = Visibility.Visible;
            area2.Visibility = Visibility.Visible;
            area3.Visibility = Visibility.Visible;
            area4.Visibility = Visibility.Visible;
            area5.Visibility = Visibility.Visible;
            area6.Visibility = Visibility.Visible;
            area7.Visibility = Visibility.Visible;
            area8.Visibility = Visibility.Visible;
            area9.Visibility = Visibility.Visible;
            area10.Visibility = Visibility.Visible;
            area11.Visibility = Visibility.Visible;
            area12.Visibility = Visibility.Visible;
            area13.Visibility = Visibility.Visible;
            area14.Visibility = Visibility.Visible;
            area15.Visibility = Visibility.Visible;
        }

        private void CreateMonsters()
        {
            monsterList = new Dictionary<Coords, Monster>(new Coords());
                        monsterList.Add(new Coords(1, 22),new Monster(r.Next(5,20), 100, monster3));
                        monsterList.Add(new Coords(2, 1), new Monster(r.Next(5, 20), 100, monster12));
                        monsterList.Add(new Coords(2, 3), new Monster(r.Next(5, 20), 100, monster11));
                        monsterList.Add(new Coords(5, 4), new Monster(r.Next(5, 20), 100, monster8));
                        monsterList.Add(new Coords(5, 14), new Monster(r.Next(5, 20), 100, monster2));
                        monsterList.Add(new Coords(6, 1), new Monster(r.Next(5, 20), 100, monster10));
                        monsterList.Add(new Coords(7, 5), new Monster(r.Next(5, 20), 100, monster9));
                        monsterList.Add(new Coords(8, 11), new Monster(r.Next(5, 20), 100, monster1));
                        monsterList.Add(new Coords(10, 3), new Monster(r.Next(5, 20), 100, monster7));
                        monsterList.Add(new Coords(11, 10), new Monster(r.Next(5, 20), 100, monster13));
                        monsterList.Add(new Coords(12, 15), new Monster(r.Next(5, 20), 100, monster14));
                        monsterList.Add(new Coords(14, 5), new Monster(r.Next(5, 20), 100, monster6));
                        monsterList.Add(new Coords(14, 18), new Monster(r.Next(5, 20), 100, monster4));
                        monsterList.Add(new Coords(16, 12), new Monster(r.Next(5, 20), 100, monster5));
                        monsterList.Add(new Coords(17, 1), new Monster(r.Next(5, 20), 100, monster15));
                        monsterList.Add(new Coords(17, 9), new Monster(r.Next(5, 20), 100, monster17));
                        monsterList.Add(new Coords(18, 7), new Monster(r.Next(5, 20), 100, monster16));
            monsterList.Add(new Coords(3, 30), new Monster(r.Next(5, 20), 100, BOSS4));
            monsterList.Add(new Coords(6, 29), new Monster(r.Next(5, 20), 100, BOSS2));
            monsterList.Add(new Coords(10, 28), new Monster(r.Next(5, 20), 100, BOSS1));
            monsterList.Add(new Coords(14, 29), new Monster(r.Next(5, 20), 100, BOSS3));
            monsterList.Add(new Coords(17, 30), new Monster(r.Next(5, 20), 100, BOSS5));
        }

        private void AttackButton_Click(object sender, RoutedEventArgs e)
        {
            fight.AttackClicked();
        }

        private void DefenseButton_Click(object sender, RoutedEventArgs e)
        {
            fight.DefenseClicked();
        }

        private void PotionButton_Click(object sender, RoutedEventArgs e)
        {
            fight.PotionClicked();
        }

        private void RunAwayButton_Click(object sender, RoutedEventArgs e)
        {
            fight.RunAwayClicked();
        }
        // Blacksmith
        private void InitializeBlacksmith()
        {
            // Grid.SetZIndex(BlacksmithGrid, -1);
            InitializeBlacksmithWeaponTypesCB();
        }
        public void UpdateBlacksmith()
        {
            if (WeaponTypesCB.SelectedItem.ToString().Equals("All"))
            {
                listBox.ItemsSource = null; // Without zeroing it wasn't working
                listBox.ItemsSource = blacksmith.Values;
            }
            else
            {
                var typeDictionary = from item
                                     in blacksmith
                                     where WeaponTypesCB.SelectedItem.ToString() == item.Value.type
                                     select item;
                List<Weapon> s = new List<Weapon>();
                foreach (var num in typeDictionary)
                {
                    s.Add(num.Value);
                }
                listBox.ItemsSource = s;

            }
            UpdateInventory();
            UpdateBlacksmithInventory();
        }
        // When other item were clicked in blacksmith list
        private void listBox_SelectionChanged(object sender, SelectionChangedEventArgs r)
        {
            if (listBox.SelectedItem != null)
            {
                BlacksmithPriceLabel.Content = "Price: " + (listBox.SelectedItem as Weapon).price;
                BlacksmithLabel.Content = (listBox.SelectedItem as Weapon).name + " of " + (listBox.SelectedItem as Weapon).enchant;
                BlacksmithStatsTextBlock.Text = (listBox.SelectedItem as Weapon).ToString();
                if(BlacksmithInventoryListBox.SelectedItem!= null)
                {
                    BlacksmithInventoryListBox.SelectedItem = null;
                }
            }
        }
        // When other item were clicked in inventory list
        private void blacksmithInventory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (BlacksmithInventoryListBox.SelectedItem != null)
            {
                BlacksmithPriceLabel.Content = "Price: " + (BlacksmithInventoryListBox.SelectedItem as Weapon).price;
                BlacksmithLabel.Content = (BlacksmithInventoryListBox.SelectedItem as Weapon).name + " of " + (BlacksmithInventoryListBox.SelectedItem as Weapon).enchant;
                BlacksmithStatsTextBlock.Text = (BlacksmithInventoryListBox.SelectedItem as Weapon).ToString();
                if(listBox.SelectedItem != null)
                {
                    listBox.SelectedItem = null;
                }
            }
        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateBlacksmith();
        }

        private void blacksmithInventoryBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateBlacksmithInventory();
        }
        private void SmithButton_Click(object sender, RoutedEventArgs e)
        {
            if (listBox.SelectedItem != null)
            {
                Weapon selectedWeapon = (listBox.SelectedItem as Weapon);
                if (game.player.coins >= selectedWeapon.price)
                {
                    string key = selectedWeapon.name;
                    inventory.Add(key, selectedWeapon);
                    blacksmith.Remove(key);
                    game.player.coins -= selectedWeapon.price;
                    UpdateBlacksmith();
                } else
                {
                    BlacksmithLabel.Content = "Not enough coins!";
                }
            }
        }

        private void MeltButton_Click(object sender, RoutedEventArgs e)
        {
            if (BlacksmithInventoryListBox.SelectedItem != null)
            {
                Weapon selectedWeapon = (BlacksmithInventoryListBox.SelectedItem as Weapon);
                string key = selectedWeapon.name;
                blacksmith.Add(key, selectedWeapon);
                inventory.Remove(key);
                game.player.coins += selectedWeapon.price;
                UpdateBlacksmith();
            }
        }
        private void InitializeBlacksmithWeaponTypesCB()
        {
            WeaponTypesCB.Items.Add("All");
            BlacksmithInventoryCB.Items.Add("All");
            foreach (string type in weaponTypes)
            {
                WeaponTypesCB.Items.Add(type);
                BlacksmithInventoryCB.Items.Add(type);
            }
        }
        
        private void UpdateBlacksmithInventory()
        {
            if (BlacksmithInventoryCB.SelectedItem != null) // No time for seeking why it becomes null at start..
            {
                if (BlacksmithInventoryCB.SelectedItem.ToString().Equals("All"))
                {
                    BlacksmithInventoryListBox.ItemsSource = null;  // Without zeroing it wasn't working
                    BlacksmithInventoryListBox.ItemsSource = inventory.Values;
                }
                else
                {
                    var typeDictionary = from item
                                         in inventory
                                         where BlacksmithInventoryCB.SelectedItem.ToString() == item.Value.type
                                         select item;
                    List<Weapon> s = new List<Weapon>();
                    foreach (var num in typeDictionary)
                    {
                        s.Add(num.Value);
                    }
                    BlacksmithInventoryListBox.ItemsSource = s;

                }
            }

        }
        private void BeerButton_click(object sender, RoutedEventArgs e)
        {
            if(game.player.coins<10)
            {
                BlacksmithLabel.Content = "Not enough gold for beer! (10 coins needed)";
            } else
            {
                game.player.coins -= 10;
                BlacksmithLabel.Content = "Beer bought!";
                game.player.haveBeer = true;
                BeerButton.Visibility = Visibility.Hidden;
            }
        }

        private void BlacksmithPotionButton_click(object sender, RoutedEventArgs e)
        {
            if (game.player.coins >= 40)
            {
                game.player.coins -= 40;
                game.player.potions++;
                BlacksmithLabel.Content = "Potion bought!";
            }
            else
            {
                BlacksmithLabel.Content = "Not enough coins!";
            }
        }

        private void minotaur_Click(object sender, RoutedEventArgs e)
        {
            wisdomMan.Minotaur();
        }

        private void riddle_Click(object sender, RoutedEventArgs e)
        {
            wisdomMan.Riddle();
        }

        private void drunkhelp_Click(object sender, RoutedEventArgs e)
        {
            wisdomMan.DrunkTip();
        }

        private void givebear_Click(object sender, RoutedEventArgs e)
        {
            if (game.player.haveBeer)
            {
                game.player.keys++;
                game.player.haveBeer = false;
            }
            givebear.Visibility = Visibility.Hidden;
            drunkMan.DrunkThanks();
        }

        private void ehm_Click(object sender, RoutedEventArgs e)
        {
            game.player.spokenToDrunkMan = true;
            ehm.Visibility = Visibility.Hidden;
            BeerButton.Visibility = Visibility.Visible;
            drunkMan.DrunkWelcome();
        }

        private void BlacksmithKeyButton_click(object sender, RoutedEventArgs e)
        {
            // Price of the key is 400, if player have this much coins, the button dissapear and he gets the key
            if(game.player.coins>=250)
            {
                BlacksmithKeyButton.Visibility = Visibility.Hidden;
                BlacksmithLabel.Content = "Key bought!";
                game.player.keys++;
                game.player.coins -= 250;
            } else
            {
                BlacksmithLabel.Content = "Not enough coins!";
            }
        }

        private void answer_Click(object sender, RoutedEventArgs e)
        {
            wisdomMan.SayIt();
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            // Exit the game
            Application.Current.Shutdown();
        }

        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            // Creating new game object
            game = new Game(this);
            // Disable main menu and display the game
            Grid.SetZIndex(MenuCanvas, -1);

            //Setting the focus here, because space could have clicked some inapropriate focused button
            buttonList[2, 7].Focus();

            // Quick tutorial
            PopupText.Text = "To move your knight click near him. Or use arrow keys. Space opens inventory.\n You can move up, down, left, right.\n Click on your knight to open inventory.";
            Grid.SetZIndex(PopupGrid, 1);
        }

        //Arrow keys handle
        internal void InitializeArrowKeys()
        {
            //We check the state of keyboard every 20 milisecond (it prevents from bugs and stack overflow)
            this.timer = new Timer();
            timer.Interval = 20;
            timer.Elapsed += new ElapsedEventHandler(timer_Tick);
            timer.AutoReset = true;
            timer.Start();
        }
        void timer_Tick(object sender, EventArgs e)
        {
            //Only from UI thread - we change the UI knight image grids
            Application.Current.Dispatcher.Invoke(new Action(() => { keyHandle(); }));   
        }

        private void keyHandle()
        {
            //Checking if some arrow is clicked
            bool left = Keyboard.IsKeyDown(Key.Left);
            bool right = Keyboard.IsKeyDown(Key.Right);
            bool up = Keyboard.IsKeyDown(Key.Up);
            bool down = Keyboard.IsKeyDown(Key.Down);
            bool space = Keyboard.IsKeyDown(Key.Space);

            // We move only in case left_enabled is true. left_enabled indicates wheter the left key was allready released
            if (left && left_enabled)
            {
                game.ButtonClicked(game.player.x, game.player.y - 1);
                left_enabled = false;
            }
            if (right && right_enabled)
            {
                game.ButtonClicked(game.player.x, game.player.y + 1);
                right_enabled = false;
            }
            if (up && up_enabled)
            {
                game.ButtonClicked(game.player.x - 1, game.player.y);
                up_enabled = false;
            }
            if (down && down_enabled)
            {
                game.ButtonClicked(game.player.x + 1, game.player.y);
                down_enabled = false;
            }
            if (space && space_enabled)
            {
                space_enabled = false;
                if (Grid.GetZIndex(InventoryGrid)==1)
                {
                    Grid.SetZIndex(InventoryGrid, -1);
                } else
                {
                    Grid.SetZIndex(InventoryGrid, 1);
                }
            }

            // Variables that indicates releasing of a button
            bool left_up = Keyboard.IsKeyUp(Key.Left);
            bool right_up = Keyboard.IsKeyUp(Key.Right);
            bool up_up = Keyboard.IsKeyUp(Key.Up);
            bool down_up = Keyboard.IsKeyUp(Key.Down);
            bool space_up = Keyboard.IsKeyUp(Key.Space);
            // If released we set them true
            if (left_up)
            {
                left_enabled = true;
            }
            if (right_up)
            {
                right_enabled = true;
            }
            if (up_up)
            {
                up_enabled = true;
            }
            if (down_up)
            {
                down_enabled = true;
            }
            if (space_up)
            {
                space_enabled = true;
            }
        }
    }
}
