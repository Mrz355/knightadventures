﻿using System.Windows.Controls;

namespace KnightAdventures
{
    public class Button1
    {
        public int x { get; set; }
        public int y { get; set; }
        public bool clicked { get; set; }
        MainWindow mainWindow;



        public Button1(int x, int y, MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
            this.x = x;
            this.y = y;

        }

        public void PressButton()
        {
            if (clicked)
            {
                Grid.SetZIndex(mainWindow.PopupGrid, 1);
                mainWindow.PopupText.Text = "It's already pressed";
            }
            else
            {
                clicked = true;
                Grid.SetZIndex(mainWindow.PopupGrid, 1);
                mainWindow.PopupText.Text = "You heared something";
            }
        }
    }
}
