﻿using System.Windows.Controls;

namespace KnightAdventures
{
    public class ClosedDoor : Door
    {
        public bool locked {get;set;}
        public ClosedDoor(int x, int y, Image img, Image area)
            : base(x, y, img, area)
        {
            locked = true;
        }

        public void UnlockDoor()
        {
            locked = false;
        }

        public void OpenClosedDoor()
        {
            if (locked == false)
            {
                OpenDoor();
            }
        }
    }
}
