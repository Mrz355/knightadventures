﻿using System.Windows.Controls;

namespace KnightAdventures
{
    public class Button2
    {
        private int[] code = new int[7];
        MainWindow mainWindow;

        public Button2(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
        }

        public bool CodeEntry(int x, int y)
        {
            if (y == 12)
            {
                Booble(1);
            }
            else if (x == 10)
            {
                Booble(0);
            }
            else
            {
                Booble(2);
            }
            return CheckCode();
        }

        private void Booble(int i)
        {
            for (int j=6;j>0;j--)
            code[j] = code[j-1];
            code[0] = i;
        }

        private bool CheckCode()
        {
            if (code[0] == 1 && code[1] == 1 && code[2] == 2 && code[3] == 0 && code[4] == 0 && code[5] == 2 && code[6] == 2)
            {
                Grid.SetZIndex(mainWindow.PopupGrid, 1);
                mainWindow.PopupText.Text = "You heared a mechanism sound";
                return true;
            }
            return false;
        }
    }
}
