﻿using System;
using System.Diagnostics;
using System.Timers;
using System.Windows;
using System.Windows.Controls;

namespace KnightAdventures
{
    internal class Fight
    {
        private Monster monster;
        private Player player;
        private MainWindow mainWindow;
        private ProgressBar playerHPBar;
        private ProgressBar monsterHPBar;
        private Label playerHPLabel;
        private Label monsterHPLabel;
        private TextBlock infoTextBlock;

        private string nextInfo;

        private FightingState fightingState;

        Random r;
        Coords coords;
        // Timer, for displaying messages in intervals
        private Timer timer;

        public Fight(Player player, Monster monster, MainWindow mainWindow, Coords coords)
        {
            this.mainWindow = mainWindow;
            this.player = player;
            this.monster = monster;
            this.coords = coords;

            InitializeElements();
        }

        private void InitializeElements()
        {
            playerHPBar = mainWindow.FightPlayerHPProgressBar;
            monsterHPBar = mainWindow.FightMonsterHPProgressBar;
            playerHPLabel = mainWindow.FightPlayerHPLabel;
            monsterHPLabel = mainWindow.FightMonsterHPLabel;
            infoTextBlock = mainWindow.FightInfoTextBlock;

            timer = new Timer();
            timer.Elapsed += new ElapsedEventHandler(TimerElapsed);
            timer.Interval = 2000;
            timer.Enabled = false;
            timer.AutoReset = false;

            r = new Random();
            
        }

        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(new Action(() => { OnTimedEvent(); }));
        }

        private void OnTimedEvent()
        {
            ChangeButtonsEnability(true);
            //System.Threading.Thread.Sleep(2000);
            switch(fightingState)
            {
                // Attack Monster!
                case FightingState.ATTACK:
                    int hit = r.Next(monster.attack/2,monster.attack);
                    infoTextBlock.Text = "Monster hit you for: " + hit +"hp.";

                    addPlayerHP(-hit);

                    if(player.hp==0)
                    {
                        infoTextBlock.Text = "You have been defeated..";
                        fightingState = FightingState.LOSE;
                        timer.Start();
                        ChangeButtonsEnability(false);
                    }
                    break;
                // Defense against monster
                case FightingState.DEFENSE:
                    int rand = r.Next(0,2);
                    hit = r.Next(monster.attack / 3);
                    if(rand == 0)
                    {
                        int hurt = r.Next(0,(-player.attack+16));
                        infoTextBlock.Text = "Monster hurted himself when trying to hit you for " + hurt +".";
                        addMonsterHP(-hurt);
                        if (monster.hp <= 0)
                        {
                            infoTextBlock.Text += " Monster is defeated! You win!";
                            fightingState = FightingState.WIN;
                            timer.Start();
                            ChangeButtonsEnability(false);
                        }
                        return;
                    }
                    infoTextBlock.Text = "Monster hit you for: " + hit + "hp.";
                    addPlayerHP(-hit);
                    if(player.hp==0)
                    {
                        infoTextBlock.Text = "You have been defeated..";
                        fightingState = FightingState.LOSE;
                        timer.Start();
                        ChangeButtonsEnability(false);
                    }
                    break;
                // Drink some potion
                case FightingState.WIN:
                    monster.img.Visibility = System.Windows.Visibility.Hidden;
                    mainWindow.fieldList[coords.x, coords.y].state = State.FLOOR;
                    Canvas.SetZIndex(mainWindow.fightCanvas, -1);
                    if(mainWindow.fieldList[3, 30].state == State.FLOOR &&
                        mainWindow.fieldList[6, 29].state == State.FLOOR &&
                        mainWindow.fieldList[10, 28].state == State.FLOOR &&
                        mainWindow.fieldList[14, 29].state == State.FLOOR &&
                        mainWindow.fieldList[17, 30].state == State.FLOOR) {
                        Grid.SetZIndex(mainWindow.PopupGrid, 1);
                        mainWindow.PopupText.Text = "Congratulations!\n\nYou win!";
                    }
                    break;
                // Flee
                case FightingState.RUNAWAY:
                    Canvas.SetZIndex(mainWindow.fightCanvas, -1);
                    break;
                case FightingState.LOSE:
                    // When you lose do something
                    player.hp = 1;
                    Canvas.SetZIndex(mainWindow.fightCanvas, -1);
                    Canvas.SetZIndex(mainWindow.MenuCanvas, 1);
                    break;
            }
        }

        internal void ChangeButtonsEnability(bool enable)
        {
            mainWindow.FightDefenseButton.IsEnabled = enable;
            mainWindow.FightAttackButton.IsEnabled = enable;
            mainWindow.FightPotionButton.IsEnabled = enable;
            mainWindow.FightRunAwayButton.IsEnabled = enable;
        }

        internal void addPlayerHP(int hit)
        {
            player.hp += hit;
            if (player.hp > 100) player.hp = 100;
            if (player.hp < 0) player.hp = 0;
            playerHPBar.Value = player.hp;
            playerHPLabel.Content = "HP: " + player.hp + "%";
        }

        internal void addMonsterHP(int hit)
        {
            monster.hp += hit;
            if (monster.hp > 100) monster.hp = 100;
            if (monster.hp < 0) monster.hp = 0;
            monsterHPBar.Value = monster.hp;
            monsterHPLabel.Content = "HP: " + monster.hp + "%";
        }

        internal void AttackClicked()
        {
            int hit = r.Next(player.attack + 1, 2*player.attack+4);

            infoTextBlock.Text = "Attack!\nYou hit for "+ hit +" hp.\n";

            addMonsterHP(-hit);
            // We win :)
            if(monster.hp <= 0)
            {
                infoTextBlock.Text += " Monster is defeated! You win!";
                fightingState = FightingState.WIN;
            }
            // We lose
            else if (player.hp <= 0)
            {
                infoTextBlock.Text += " You have been defeated..";
                fightingState = FightingState.LOSE;
            }
            else
            {
                fightingState = FightingState.ATTACK;
                // Monster prepares his attack
            }
            timer.Start();
            ChangeButtonsEnability(false);
        }

        internal void DefenseClicked()
        {
            fightingState = FightingState.DEFENSE;

            infoTextBlock.Text = "You raise your shield to defense.";

            timer.Start();
            ChangeButtonsEnability(false);
        }

        internal void PotionClicked()
        {
            // If we've got some potions
            if (player.potions > 0)
            {
                fightingState = FightingState.ATTACK;

                int hpRestored = r.Next(25, 45);

                addPlayerHP(hpRestored);

                player.potions--;

                infoTextBlock.Text = "You use potion.\nYou restored " + hpRestored + " hp.";

                timer.Start();
                ChangeButtonsEnability(false);
            } else
            // No potions.. 
            {
                infoTextBlock.Text = "You haven't got any potion!";
            }
        }

        internal void RunAwayClicked()
        {
            infoTextBlock.Text = "You flee..";
            fightingState = FightingState.RUNAWAY;
            timer.Start();
            ChangeButtonsEnability(false);
        }
    }
    enum FightingState
    {
        ATTACK, DEFENSE, POTION, RUNAWAY, WIN, LOSE
    }
}