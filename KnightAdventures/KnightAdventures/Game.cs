﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;

namespace KnightAdventures
{
    class Game
    {
        private MainWindow mainWindow;
        public Player player { get; set; }
        private Field[,] fieldList;
        private Dictionary <string, Weapon> blacksmith;
        private Dictionary <string, Weapon> lootWeapons;
        private Dictionary <string, Weapon> inventory;
        private List<Door> doors = new List<Door>();
        private List<Button1> buttons1 = new List<Button1>();
        private Button2 button2;
        private List<ClosedDoor> closedDoors = new List<ClosedDoor>();
        private List<string> weaponNames;
        private Grid lootGrid;
        private Grid inventoryGrid;
        private Grid wisdomGrid;
        private Grid drunkGrid;
        private Grid PopupGrid;
        private Canvas fightCanvas;
        private Coords coords = new Coords();
        private Grid blacksmithGrid;

        private Random r = new Random();

        public Game(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
            this.fieldList = mainWindow.fieldList;
            this.blacksmith = mainWindow.blacksmith;
            this.weaponNames = mainWindow.weaponNames;
            this.lootGrid = mainWindow.LootGrid;
            this.inventoryGrid = mainWindow.InventoryGrid;
            this.wisdomGrid = mainWindow.WisdomGrid;
            this.drunkGrid = mainWindow.DrunkGrid;
            this.doors = mainWindow.doors;
            this.buttons1 = mainWindow.buttons1;
            this.closedDoors = mainWindow.closedDoors;
            this.fightCanvas = mainWindow.fightCanvas;
            this.button2 = mainWindow.button2;
            this.blacksmithGrid = mainWindow.BlacksmithGrid;
            this.lootWeapons = mainWindow.lootWeapons;
            this.PopupGrid = mainWindow.PopupGrid;
            player = new Player(mainWindow);
            // Game started, so we need to turn on arrow keys
            mainWindow.InitializeArrowKeys();
        }

        // Some button was clicked. This method takes coords of this button as arguments and handle this click
        public void ButtonClicked(int x, int y)
        {

            if (Grid.GetZIndex(lootGrid)==1 || Grid.GetZIndex(inventoryGrid)==1 || Grid.GetZIndex(wisdomGrid) == 1 || Grid.GetZIndex(drunkGrid) == 1 || Grid.GetZIndex(blacksmithGrid) == 1 || Grid.GetZIndex(PopupGrid) == 1)
            {
                Grid.SetZIndex(lootGrid, -1);
                Grid.SetZIndex(inventoryGrid, -1);
                Grid.SetZIndex(wisdomGrid,-1);
                Grid.SetZIndex(drunkGrid, -1);
                Grid.SetZIndex(blacksmithGrid, -1);
                Grid.SetZIndex(PopupGrid, -1);
            }
            // If we clicked ourselves
            if (player.x == x && player.y == y)
            {
                // We show inventory and stats
                ShowInventory();
            }
            // If we clicked button in nearby area
            else if((player.x+1==x && player.y == y) || (player.x-1==x && player.y == y) || (player.y+1==y && player.x == x) || (player.y-1 == y && player.x == x))
            {
                //Then Handle interaction
                // If we clicked a floor move our character
                if (fieldList[x,y].state==State.FLOOR) 
                {
                    player.setPosition(x, y);
                }
                // If we clicked on the opened_door - open it
                else if (fieldList[x, y].state == State.OPENED_DOOR)
                {
                    OpenDoor(x, y);
                }
                // If we clicked allready looted crate, show adequate message
                else if (fieldList[x, y].state == State.LOOTED)
                {
                    LootedClicked();
                }
                // If we clicked loot then check for items
                else if (fieldList[x,y].state==State.LOOT) 
                {
                    LootClicked();
                    fieldList[x, y].state = State.LOOTED;
                }
                // If we clicked enemy, then prepare to fight
                else if (fieldList[x,y].state==State.ENEMY)
                {
                    FightClicked(x, y); // Off it in CHEAT MODE
                    //player.setPosition(x, y); //CHEAT MODE
                }
                else if (fieldList[x, y].state == State.BUTTON1)
                {
                    Button1Clicked(x,y);
                } else if (fieldList[x,y].state==State.SELLER_MAN)
                {
                    SellerClicked();
                }
                else if (fieldList[x, y].state == State.BUTTON2)
                {
                    Button2Clicked(x, y);
                }
                else if (fieldList[x, y].state == State.CLOSED_DOOR)
                {
                    TryOpenDoor(x, y);
                }
                else if (fieldList[x, y].state == State.WISDOM_MAN)
                {
                    WisdomTalk();
                }
                else if (fieldList[x, y].state == State.DRUNK_MAN)
                {
                    DrunkTalk();
                }
                else if (fieldList[x, y].state == State.KEYLOOT)
                {
                    LootKey();
                    fieldList[x, y].state = State.LOOTED;
                }

            } 
            else
            {
                //player.setPosition(x, y); //CHEATMODE
                // If we clicked some random, far button, do nothing
            }
        }

        private void SellerClicked()
        {
            mainWindow.UpdateBlacksmith();
            if(player.spokenToDrunkMan)
            {
                mainWindow.BeerButton.Visibility = System.Windows.Visibility.Visible;
            }

            Grid.SetZIndex(mainWindow.BlacksmithGrid, 1);
            
        }

        private void FightClicked(int x, int y)
        {
            coords.x = x;
            coords.y = y;
            Monster monster = mainWindow.monsterList[new Coords(x,y)];

            mainWindow.FightMonsterHPProgressBar.Value = monster.hp;
            mainWindow.FightMonsterHPLabel.Content = "HP " + monster.hp + "%";
            mainWindow.FightInfoTextBlock.Text = "Wild monster appears..";

            Canvas.SetZIndex(fightCanvas, 1);

            mainWindow.fight = new Fight(player,monster,mainWindow,coords);
        }

        private void ShowInventory()
        {
            Grid.SetZIndex(inventoryGrid, 1);
        }
        private void WisdomTalk()
        {
            Grid.SetZIndex(wisdomGrid, 1);
            mainWindow.wisdomMan.SetPlayer(player);
            mainWindow.WisdomText.Text = "Hello warrior! How can I help you?";
            mainWindow.textBox.Visibility = System.Windows.Visibility.Hidden;
            mainWindow.answer.Visibility = System.Windows.Visibility.Hidden;
        }
        private void DrunkTalk()
        {
            Grid.SetZIndex(drunkGrid, 1);
            if(player.haveBeer)
            {
                mainWindow.givebear.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void LootKey()
        {
            player.keys++;
            Grid.SetZIndex(PopupGrid, 1);
            mainWindow.PopupText.Text = "You found a key!";
        }

        private void LootedClicked()
        {
            Grid.SetZIndex(lootGrid, 1);
            mainWindow.LootText.Text = "This crate is empty.";
        }
        private void LootClicked()
        {
            Grid.SetZIndex(lootGrid, 1);
            int loot = r.Next(9);
            // The most chance we've got to loot weapon
            if (loot <= 3)
            {
                String weaponName = mainWindow.GetRandomElement(lootWeapons.Keys.ToList<string>());
                Weapon weapon = lootWeapons[weaponName];
                lootWeapons.Remove(weaponName);
                //weaponNames.Remove(weaponName);
                mainWindow.LootText.Text = "You found:\n" + weapon.ToString();
                player.addToInventory(weaponName, weapon);
            }
            // Sometimes we can loot potions
            else if (loot == 4 || loot == 5)
            {
                mainWindow.LootText.Text = "You found " + (loot - 3) + " potions."; // You can loot one or two potions
                player.potions+=(loot-3);
            }
            // Sometimes coins
            else if (loot == 6 || loot == 7)
            {
                int coins = r.Next(15, 50);
                mainWindow.LootText.Text = "You found " + coins + " coins.";
                player.coins += coins; 
            }
            // Very rare but still occurs - empty case
            else
            {
                mainWindow.LootText.Text = "This crate is empty.";
            }
        }

        private void Button1Clicked(int x, int y)
        {
            foreach (Button1 b in buttons1)
            {
                if (b.x == x && b.y == y)
                    b.PressButton();
            }
            CheckButtons1();
        }

        private void CheckButtons1()
        {
            int i=0;
            foreach (Button1 b in buttons1)
            {
                if (b.clicked)
                    ++i;
            }
            if (i == 5)
            {
                closedDoors[1].UnlockDoor();
                Grid.SetZIndex(mainWindow.PopupGrid, 1);
                mainWindow.PopupText.Text = "You heared a mechanism sound";
            }
        }

        private void OpenDoor(int x, int y)
        {
            foreach (Door d in doors)
            {
                if (d.x == x && d.y == y)
                    d.OpenDoor();
            }
            fieldList[x, y].state = State.FLOOR;
        }

        private void Button2Clicked(int x, int y)
        {
            if (button2.CodeEntry(x, y))
            {
                closedDoors[2].UnlockDoor();
            }
        }

        private void TryOpenDoor(int x,int y)
        {
            foreach (ClosedDoor d in closedDoors)
            {
                if (d.x == x && d.y == y)
                {
                    if (d.locked == false)
                    {
                        d.OpenClosedDoor();
                        fieldList[x, y].state = State.FLOOR;
                    }
                    else
                    {
                        if (player.keys == 5)
                        {
                            d.locked = false;
                            Grid.SetZIndex(PopupGrid, 1);
                            mainWindow.PopupText.Text = "You have used keys to open the door";
                        }
                        else
                        {
                            Grid.SetZIndex(PopupGrid, 1);
                            mainWindow.PopupText.Text = "It looks like door, but it's closed";
                        }
                    }
                    
                }
            }
            
        }
    }
}
