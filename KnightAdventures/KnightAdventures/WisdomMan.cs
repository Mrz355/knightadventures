﻿namespace KnightAdventures
{
    public class WisdomMan
    {
        private MainWindow mainWindow;
        private Player player;
        private bool haveKey;

        public WisdomMan(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
            haveKey = true;
        }

        public void SetPlayer(Player player)
        {
            this.player = player;
        }

        public void DrunkTip()
        {
            mainWindow.WisdomText.Text = "Oh have you met Ryszard yet?\nNear the stained glass window there is slightly damaged wall. \nRyszard enter that way with his \"Joanna\" to board a \"plane\"";
        }

        public void Minotaur()
        {
            mainWindow.WisdomText.Text = "Have you heard the story about the Minoraur? \n The Demonic Minotaur\nThe one mighty creature that hides in those dungeons.\n Many warriors have tried to defeat it, but non of them has succeded. \n After killing hundreds of warriors, Minotaur hid himself for some time in the chamber filled with snakes, in order to during previous Snake's year come back even stronger.\nStatues tell the story";
        }

        public void Riddle()
        {
            if (haveKey)
            {
                mainWindow.WisdomText.Text = "Sure, I can tell that you need five keys.\n I can give you one, but not for free.\n Tell me what disappears the moment you say its name?";
                mainWindow.textBox.Visibility = System.Windows.Visibility.Visible;
                mainWindow.answer.Visibility = System.Windows.Visibility.Visible;
            }
            else
                mainWindow.WisdomText.Text = "I can only tell that you need five keys.\n I don't have any more keys.";
        }

        public void SayIt()
        {
            if (mainWindow.textBox.Text.ToUpper() == "SILENCE")
            {
                haveKey = false;
                mainWindow.WisdomText.Text = "Great! Here, take that key";
                player.keys++;
                mainWindow.textBox.Visibility = System.Windows.Visibility.Hidden;
                mainWindow.answer.Visibility = System.Windows.Visibility.Hidden;
            }
            else
            {
                mainWindow.WisdomText.Text = "You are wrong! Think for a moment.\n And tell me what disappears the moment you say its name?";
            }
        }
    }
}
