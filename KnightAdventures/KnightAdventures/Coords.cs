﻿using System;
using System.Collections.Generic;

namespace KnightAdventures
{
    class Coords : IEqualityComparer<Coords>
    {
        internal int x;
        internal int y;

        public Coords() { }

        public Coords(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
        
        override
        public bool Equals(Object o)
        {
            if (o == null) return false;
            if (!(this.GetType() == o.GetType())) return false;
            Coords c = (Coords)o;
            return c.x == this.x && c.y == this.y;
        }

        public bool Equals(Coords x, Coords y)
        {
            return x.x == y.x && x.y == y.y;
        }

        public int GetHashCode(Coords obj)
        {
            return 1000*obj.x + obj.y;
        }
    }
}
