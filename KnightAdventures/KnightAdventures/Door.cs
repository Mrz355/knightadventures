﻿using System.Windows.Controls;

namespace KnightAdventures
{
    public class Door
    {
        public int x { get; set; }
        public int y { get; set; }
        private Image img, area;

        public Door(int x, int y, Image img, Image area)
        {
            this.x = x;
            this.y = y;
            this.img = img;
            this.area = area;
        }

        public void OpenDoor()
        {
            img.Visibility = System.Windows.Visibility.Hidden;
            area.Visibility = System.Windows.Visibility.Hidden;
        }

            
    }
}
