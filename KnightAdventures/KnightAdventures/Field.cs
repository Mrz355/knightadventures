﻿namespace KnightAdventures
{
    public class Field
    {
        public State state { get; set; }
        public bool isClickable { get; set; }

        public Field()
        {
            state = State.FLOOR;
            isClickable = false;
        }
    }
}
